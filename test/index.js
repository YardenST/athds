const assert = require('assert');

/**
 * CHECK Data Layer
 */
const healthChecksDB = require('./../src/dal');

const db = healthChecksDB;

const service1 = 'service1';
assert(db.isHealthy(service1) === null); //status is unknown
assert(db.serviceAvailabilityPercentage(service1) === null);

db.addRecord({isHealthy: true, name: service1});
assert(db.isHealthy(service1) === true); //healthy
assert(db.serviceAvailabilityPercentage(service1) === 1);

db.addRecord({isHealthy: false, name: service1});
assert(db.isHealthy(service1) === false);//unhealthy
assert(db.serviceAvailabilityPercentage(service1) === 0.5);

db.addRecord({isHealthy: true, name: service1});
assert(db.isHealthy(service1) === true); //healthy
assert(db.serviceAvailabilityPercentage(service1) === 2 / 3);

/**
 * Checks that the db saves only last 60 records.
 */

for (let i = 0; i <= 65; i++) {
  db.addRecord({isHealthy: true, name: service1});
}
assert(db.serviceAvailabilityPercentage(service1) === 1);

for (let i = 0; i <= 59; i++) {
  db.addRecord({isHealthy: false, name: service1});
}
assert(db.serviceAvailabilityPercentage(service1) === 0);

for (let i = 0; i <= 59; i++) {
  db.addRecord({isHealthy: i % 2 === 0, name: service1});
}
assert(db.serviceAvailabilityPercentage(service1) === 0.5);

/**
 * monitoring
 */

const mirror         = async ({body}) => body;
const monitorService = require('./../src/monitoring-service/monitor-service')(mirror);

monitorService({
  name: 'myService', body: 'res', testFunction: (res, cb) => {
    cb(null, true);
  }
}, (error, isHealthy) => {
  assert(isHealthy === true);
});

monitorService({
  name: 'myService', body: 'res', testFunction: (res, cb) => {
    cb(null, false);
  }
}, (error, isHealthy) => {
  assert(isHealthy === false);
});

monitorService({
  name: 'myService', body: 'foo', testFunction: (res, cb) => {
    cb(null, res === 'foo');
  }
}, (error, isHealthy) => {
  assert(isHealthy === true);
});

monitorService({
  name: 'myService', body: 'foo', testFunction: (res, cb) => {
    cb(null, res === 'bar');
  }
}, (error, isHealthy) => {
  assert(isHealthy === false);
});

//todo add tests to the Monitor class itself.. but it is really simple so I skipped it.
//todo add tests for the actual responses Autodesk provided...I took it out to conf file to I assume the developer
// tested his test function by himself..I only deal with the actual monitoring

console.log('tests are successful');