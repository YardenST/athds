**System Design**

1. The Monitoring Service: It is responsible to monitor a list of given services and report back to the DB
2. The DB: In charge to save the results and provide some methods to query it
3. The HTTP Service: Provides an easy way to access the data

**What I had in mind**

The system was built such it will be easy to add more services easily. 
This is why the only configuration file is the `src/services/index.js`.
The system is agnostic about how to determine if a service is healthy or not
The end user should create its own test function.

**System Parameters**

There are 2 other important parameters: The sampling rate and the DB memory size (how many records it saves per service)
You can play with 2 parameters to change the configuration of the system.
It is now set so you can see the availability for the last 60 minutes in 1m intervals.

**Good to Know**

Once you start the service, first results arrive after 60 seconds.
If you see `null` in the API response, it means no data is available.

I've added some *todos* in the code of parts I know are not perfect and should be improved.

In each folder, the `index.js` file is the important file, which uses other parts of the code within the folder.

**How to run:**

`npm install`

`npm start`

**Run tests:**

`npm test` 

**How to configure:**

visit the `src/services/index.js` to add more services