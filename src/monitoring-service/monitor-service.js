//im using cb because the xml2js package uses cb instead of direct style..
//I could invest more time and do both options


const monitorService = (fetchService) => async (serviceConf, cb) => {
  try {
    const res = await fetchService(serviceConf);
    serviceConf.testFunction(res, cb);
  } catch (e) {
    console.log(`error fetching the service ${serviceConf.name} or testing it. Defaulting to false.`);
    console.error(e);
    cb(null, false);
  }
};

module.exports = monitorService;