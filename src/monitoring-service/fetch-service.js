const rp = require('request-promise');

/**
 * tries to fetch the given resource. If failed returns null.
 * @param {ServiceConf} service
 * @return {Promise<null|Object>}
 */
const fetchUrl = async (service) => {
  try {
    return await rp(service.url);//todo set a timeout for this request.
  } catch (e) {
    return null;
  }
};

module.exports = fetchUrl;