const EventEmitter   = require('events');
const fetchUrl       = require('./fetch-service');
const monitorService = require('./monitor-service')(fetchUrl);

/**
 * @typedef {Object} ServiceConf
 * @property {String} name
 * @property {String} url
 * @property {function(String, function(Object,Boolean): void): Boolean} testFunction
 */


const validateService = (serviceConf) => {
  if (!(serviceConf.name && typeof (serviceConf.name) === 'string' && serviceConf.name.length > 0)) {
    throw new Error('service must have a name property which is a non empty string');
  }
  try {
    new URL(serviceConf.url);
  } catch (e) {
    throw new Error('service must have a valid url property');
  }
  if (!(serviceConf.testFunction && typeof (serviceConf.testFunction) === 'function' && serviceConf.testFunction.length === 2)) {
    throw new Error('service must have a testFunction property which is a function with 2 parameters: the service response and a callback that takes an error and boolean which indicates if the service response is healthy');
  }
};

/**
 * This class gets as an input services configurations, and emits events every time there is a new health check
 * result
 */
class Monitor extends EventEmitter {
  constructor(samplingRateMS = 60 * 1000) {
    super();
    this.samplingRateMS = samplingRateMS;
    /** @type {Array<ServiceConf>}*/
    this.services = [];
  }

  /**
   * adds a new service for monitoring
   * @param {ServiceConf} serviceConf
   */
  addService(serviceConf) {
    validateService(serviceConf);
    this.services.push(serviceConf);
    return this;
  }


  /**
   * starts to monitor the given services.
   */
  start() {
    setInterval(this.__monitorServices.bind(this), this.samplingRateMS);
  }


  __monitorServices() {
    for (const service of this.services) {
      monitorService(service, (error, isHealthy) => {
        if (error) {
          this.emit('error', e);
          return;
        }
        this.emit('result', {name: service.name, url: service.url, isHealthy})
      }).catch((error) => this.emit('error', e));
    }
  }
}

module.exports = Monitor;