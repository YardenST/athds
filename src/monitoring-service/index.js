const Monitor        = require('./monitor');
const healthChecksDB = require('./../dal');
const services       = require('./../services');

const monitor = new Monitor(60 * 1000); //this configuration makes the api work as the assignment wants it to.

for (const service of services) {
  monitor.addService(service);
}

monitor.on('result', ({name, isHealthy}) => {
  healthChecksDB.addRecord({name, isHealthy});
  console.log(`new result for ${name} isHealthy: ${isHealthy}`);
}).on('error', (error) => {
  console.log(`unexpected error monitoring a service`);
  console.error(error);
});

monitor.start();