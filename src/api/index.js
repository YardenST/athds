const express = require('express');

const servicesRouter = require('./services');

const index = express();

index.use(express.json());
index.use(express.urlencoded({extended: false}));

index.use('/v1/services', servicesRouter);

module.exports = index;
