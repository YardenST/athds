const express        = require('express');
const router         = express.Router();
const servicesConf   = require('./../services');
const healthChecksDB = require('./../dal');

const currentStatus       = (req, res, next) => {
  const result = {};
  for (const serviceConf of servicesConf) {
    result[serviceConf.name] = {url: serviceConf.url, isHealthy: healthChecksDB.isHealthy(serviceConf.name)}
  }
  res.json(result);
  next();
};
const currentAvailability = (req, res, next) => {
  const result = {};
  for (const serviceConf of servicesConf) {
    result[serviceConf.name] = {
      url         : serviceConf.url,
      availability: healthChecksDB.serviceAvailabilityPercentage(serviceConf.name)
    }
  }
  res.json(result);
  next();
};
router.get(`/status`, currentStatus);
router.get(`/availability`, currentAvailability);


module.exports = router;
