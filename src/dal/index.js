/**
 * This class is responsible to save the monitoring checks. and provide some useful method to query them
 */
class HealthChecksDB {
  constructor() {
    this.data = new Map();
  }

  /**
   * adds a new record for a service
   * @param {String} name the service identifier
   * @param {Boolean} isHealthy the result
   */
  addRecord({name, isHealthy}) {
    const records = this.data.get(name) || [];
    records.push(isHealthy);
    if (records.length > 60) { //todo take this 60 out and make this class more generic. Or save each record with its timestamp for better analysis
      records.shift();
    }
    this.data.set(name, records);
  }

  /**
   * Given a service name, returns its latest health result
   * @param name the service name
   * @return {null|Boolean} returns the latest health status result. If not exists, returns null (which is to say: unknown status)
   */
  isHealthy(name) {
    const records = this.data.get(name);
    if (!records) {
      return null;
    }
    return records[records.length - 1];
  }

  /**
   *
   * @param name the service name
   * @return {null|Number} if not checks are available, returns null (state is unknown) otherwise returns the ratio between
   * trues and overall checks
   */
  serviceAvailabilityPercentage(name) {
    const records = this.data.get(name);
    if (!records) {
      return null;
    }
    return records.filter(Boolean).length / records.length;
  }


}

module.exports = new HealthChecksDB();

