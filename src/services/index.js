const parseXmlString = require('xml2js').parseString;

const service1Conf = {
  name        : 'poor service',
  url         : 'https://bim360dm-dev.autodesk.com/health?self=true',
  testFunction: (res, cb) => {cb(null, res !== null)}
};


const jsonServiceConf = {
  name        : 'json service',
  url         : 'https://commands.bim360dm-dev.autodesk.com/health',
  testFunction: (res, cb) => {
    try {
      const json = JSON.parse(res);
      cb(null, !!(json && json.status && json.status.overall === 'OK'));
    } catch (e) {
      cb(null, false);
    }
  }
};


const xmlServiceConf = {
  name        : 'xml service',
  url         : 'https://360-staging.autodesk.com/health',
  testFunction: (res, cb) => {
    try {
      parseXmlString(res, (e, r) => {
        if (e) {
          cb(null, false);
          return;
        }
        cb(null, r && r.HealthCheck && r.HealthCheck.status && r.HealthCheck.status[0] === 'Good');
      });
    } catch (e) {
      cb(null, false);
    }
  }
};

module.exports = [xmlServiceConf, jsonServiceConf, service1Conf];